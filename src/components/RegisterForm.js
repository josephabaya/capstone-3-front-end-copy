import {Form , Button} from 'react-bootstrap'
import { useState } from 'react'

export default function RegisterForm({setIsRedirect}){

	const [ newCredentials, setNewCredentials ] = useState({
		firstName: "",
	    lastName: "", 
	    email: "",
	    password: "", 
	    confirmPassword: ""	
	})

	const [ isLoading, setIsLoading ] = useState(false)

	const handleSubmit = e => {
		e.preventDefault()
		setIsLoading(true)
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/register', {
			method: "POST",
			body: JSON.stringify(newCredentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then(res => {
			console.log(res)
			setIsLoading(false)
			if( res.status === 200){
				alert( "Account Created" )
				return res.json()
			} else {
				alert("Invalid Credentials")
				throw new Error('Complete Fields Require')
			}
		})
		.then( data => {
			setIsRedirect(true)
		})
		.catch(err => console.log(err))
	}

	const handleChange = e => {
		setNewCredentials({
			...newCredentials,
			[e.target.id] : e.target.value
		})
	}

	return(

		<Form onSubmit={handleSubmit}>

				<Form.Group controlId="firstName">
						<Form.Label>First Name: </Form.Label>
						<Form.Control 
							type="text"
							onChange={handleChange}
							value={newCredentials.firstName}
						/>
				</Form.Group>


				<Form.Group controlId="lastName">
						<Form.Label>Last Name: </Form.Label>
						<Form.Control 
							type="text"
							onChange={handleChange}
							value={newCredentials.lastName}
						/>
				</Form.Group>


				<Form.Group controlId="email">
						<Form.Label>Email: </Form.Label>
						<Form.Control 
							type="email"
							onChange={handleChange}
							value={newCredentials.email}
						/>
				</Form.Group>


				<Form.Group controlId="password">
						<Form.Label>Password: </Form.Label>
						<Form.Control 
							type="password" 
							onChange={handleChange}
							value={newCredentials.password}
						/>
				</Form.Group>


				<Form.Group controlId="confirmPassword">
						<Form.Label>Confirm Password: </Form.Label>
						<Form.Control 
							type="password"
							onChange={handleChange}
							value={newCredentials.confirmPassword}
						/>
				</Form.Group>



				{ isLoading ? 
					<Button type="submit" block disabled>Create Account</Button>
				: 	<Button type="submit" >Create Account</Button>
				}
		</Form>	


	)

}