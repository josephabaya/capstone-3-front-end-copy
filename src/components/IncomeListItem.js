import { Button, Card } from 'react-bootstrap';
import { ApplicationContext } from './../contexts/ApplicationContext'
import { Link } from 'react-router-dom'

export default function IncomeListItem({ profit, viewBtn, setLastDeletedIncome}){


	const handleClickDelete = () => {
		console.log(profit._id)
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/entry/${profit._id}`,{
			method : 'DELETE',
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then( res => res.json())
		.then( data => {
			setLastDeletedIncome(data)
		})
		.catch(err => console.log(err))
	}


	return(
		<Card >
		  <Card.Body>
		    <Card.Title>{profit.category}</Card.Title>
		    <Card.Text>{profit.transaction}</Card.Text>
		    <Card.Text>{profit.amount}</Card.Text>

		   { 
			   	viewBtn ?
			   	<Button
		   		    as={Link}
		   			to={`/profit/${profit._id}`}
		   		    className="my-1" 
		   		    variant="primary" 
		   		    block
	   		    >View</Button>
	   		    : <></>
		   	}

		    {
		    	viewBtn ?
		    	<Button 
		    	onClick={handleClickDelete}
		    	className="my-1" 
		    	variant="danger" 
		    	block
		    	>Delete</Button>
		    	: <></>
		    }


		  </Card.Body>
		</Card>
	)
}