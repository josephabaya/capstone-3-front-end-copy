import { Card, Form, Button } from 'react-bootstrap'
import { useState } from 'react'


const CategoryListUpdate = ({ categ , setLastUpdatedCategory }) => {


	const [ currentCategory, setCurrentCategory ] = useState({ })

	const handleChange = e =>{
		setCurrentCategory({
		...currentCategory,
		[e.target.id] : e.target.value		
		})
	}

	const handleSubmit = (e) => {
		e.preventDefault()

		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/category/${categ._id}`,{
			method: "PUT",
			body: JSON.stringify(currentCategory),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem('token')}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			setLastUpdatedCategory(data)
		})
		.catch(err => console.log(err))
	}


	return(
		<Card.Body>
			<Form onSubmit={ handleSubmit } >
				<Form.Group controlId="category">
					<Form.Label>Category: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>

				<Form.Group controlId="description">
					<Form.Label>Description: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>
			
				<Button type="submit" >Update</Button>
			</Form>
		</Card.Body>		
	)
}

export default CategoryListUpdate;