import { Container, Card,Row, Col } from 'react-bootstrap'
// import { useContext } from 'react'
import { useEffect , useState } from 'react' 
import CategoryListItem from './CategoryListItem'

import CategoryCreate from './../components/CategoryCreate'


export default function CategoryList(){

	let [ category, setCategory ] = useState([])
	const [ lastDeletedProduct, setLastDeletedProduct ] = useState([])
	const [ lastCreatedCategory , setLastCreatedCategory ] = useState([])

	useEffect(() => {
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/category',{
				headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then( res => res.json())
		.then( data =>{
			setCategory(data)
		})
		.catch( err => console.log(err))
	},[lastCreatedCategory, lastDeletedProduct])

	const categoryDisplay = category.map( categ =>{
		return(
			<Col className="my-2" xs={12} sm={6} md={4} lg={3} key={categ._id}>
				<CategoryListItem
				categ={categ}
				setLastDeletedProduct={setLastDeletedProduct}
				viewBtn={true}
				/>
			</Col>
		)
	})


	return(
		<Container className="my-5">
			
			<Row>
				{categoryDisplay}
			</Row>
			

			<Card className="my-5">
				<Row>
					<Col>
						<CategoryCreate setLastCreatedCategory={setLastCreatedCategory}/>
					</Col>
				</Row>
			</Card>
		</Container>
	)
}


