import { Container,Row, Col } from 'react-bootstrap'
import { useEffect, useState } from 'react' 
import { useParams } from 'react-router-dom'
import IncomeListItem from './IncomeListItem'

export default function IncomeList(){


	const [ income, setIncome ] = useState([])
	const [ lastDeletedIncome, setLastDeletedIncome ] = useState([])


	useEffect(() => {
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/income',{
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setIncome(data)
		})
		.catch(err => console.log(err))
	},[lastDeletedIncome])

	const incomeDisplay = income.map( profit => {
		return(
			<Col className="my-2" xs={12} sm={6} md={4} lg={3} key={profit._id}>
				<IncomeListItem 
				profit={profit} 
				setLastDeletedIncome={setLastDeletedIncome}
				viewBtn={true}
				/>
			</Col>
		)
	})


	return(
		<Container className="my-5">
			<Row>
				{ incomeDisplay }
			</Row>
		</Container>	
	)
}