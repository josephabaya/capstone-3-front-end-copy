import { Card, Form, Button } from 'react-bootstrap'
import { useState } from 'react'


const IncomeListUpdate = ({ profit, setLastUpdatedIncome }) => {

	const [ currentIncome, setCurrentIncome ] = useState({})

	const handleChange = e =>{
		setCurrentIncome({
		...currentIncome,
		[e.target.id] : e.target.value		
		})
	}

	const handleSubmit = (e) => {
		e.preventDefault()

		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/transaction/${profit._id}`,{
			method: "PUT",
			body: JSON.stringify(currentIncome),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem('token')}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setLastUpdatedIncome(data)
		})
		.catch(err => console.log(err))
	}


	return(
		<Card.Body>
			<Form onSubmit={ handleSubmit } >
{/*				<Form.Group controlId="category">
					<Form.Label>Category: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>*/}

				<Form.Group controlId="transaction">
					<Form.Label>Transaction: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>

				<Form.Group controlId="amount">
					<Form.Label>Amount: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>
			
				<Button type="submit" >Update</Button>
			</Form>
		</Card.Body>		
	)

}


export default IncomeListUpdate;

