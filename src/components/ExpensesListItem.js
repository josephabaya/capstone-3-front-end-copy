import { Button, Card } from 'react-bootstrap';
import { ApplicationContext } from './../contexts/ApplicationContext'
import { Link } from 'react-router-dom'

export default function ExpensesListItem({ loss, viewBtn, setLastDeletedIncome }){

	const handleClickDelete = () => {
		console.log(loss._id)
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/entry/${loss._id}`,{
			method : 'DELETE',
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then( res => res.json())
		.then( data => {
			setLastDeletedIncome(data)
		})
		.catch(err => console.log(err))
	}


	return(
		<Card >
		  <Card.Body>
		    <Card.Title>{loss.category}</Card.Title>
		    <Card.Text>{loss.transaction}</Card.Text>
		    <Card.Text>{loss.amount}</Card.Text>
		    
		   {
		   		viewBtn ?
			   	<Button
			   	as={Link}
			   	to={`/loss/${loss._id}`}
	   		    className="my-1"
	   		    variant="primary"
	   		    block
	   		    >View</Button>
	   		    : <></>
		   }
		   {
			    viewBtn ?
			    <Button
			    onClick={handleClickDelete}
			    className="my-1"
			    variant="danger"
			    block
			    >Delete</Button>
			    : <></>
		    }




		  </Card.Body>
		</Card>
	)
}