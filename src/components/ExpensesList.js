import { Container,Row, Col } from 'react-bootstrap'
import { useEffect, useState } from 'react' 
import { useParams } from 'react-router-dom'
import ExpensesListItem from './ExpensesListItem'

export default function ExpensesList(){


	const [ expenses, setExpenses ] = useState([])
	const [ lastDeletedExpenses, setLastDeletedExpenses ] = useState([])


	useEffect(() => {
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/expenses',{
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setExpenses(data)
		})
		.catch(err => console.log(err))
	},[lastDeletedExpenses])

	const expensesDisplay = expenses.map( loss => {
		return(
			<Col className="my-2" xs={12} sm={6} md={4} lg={3} key={loss._id}>
				<ExpensesListItem
				loss={loss}
				lastDeletedExpenses={lastDeletedExpenses}
				viewBtn={true}
				/>
			</Col>
		)
	})


	return(
		<Container className="my-5">
			<Row>
				{ expensesDisplay }
			</Row>
		</Container>	
	)
}