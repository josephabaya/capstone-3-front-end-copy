import { Card, Form, Button } from 'react-bootstrap'
import { useState } from 'react'


const ExpensesListUpdate = ({ loss , setLastUpdatedExpenses}) => {

	const [ currentExpenses, setCurrentExpenses ] = useState({})

	const handleChange = e =>{
		setCurrentExpenses({
		...currentExpenses,
		[e.target.id] : e.target.value		
		})
	}

	const handleSubmit = (e) => {
		e.preventDefault()

		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/transaction/${loss._id}`,{
			method: "PUT",
			body: JSON.stringify(currentExpenses),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem('token')}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			setLastUpdatedExpenses(data)
		})
		.catch(err => console.log(err))
	}


	return(
		<Card.Body>
			<Form onSubmit={ handleSubmit } >
				{/*<Form.Group controlId="category">
					<Form.Label>Category: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>*/}

				<Form.Group controlId="transaction">
					<Form.Label>Transaction: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>

				<Form.Group controlId="amount">
					<Form.Label>Amount: </Form.Label>
					<Form.Control type="text" onChange={handleChange}/>
				</Form.Group>
			
				<Button type="submit" >Update</Button>
			</Form>
		</Card.Body>		
	)

}


export default ExpensesListUpdate;

