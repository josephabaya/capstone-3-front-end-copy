import { useState } from 'react'
import { Card, Form, Button } from 'react-bootstrap'

const CategoryCreate = ({setLastCreatedCategory}) =>{

	const [ newCategoryInput, setNewCategoryInput ] = useState({
		"category" : "",
    	"description" : ""
	})

	const [ isLoading, setIsLoading ] = useState(false)

	const handleSubmit = e => {
		e.preventDefault()
		setIsLoading(true)
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/category',{
			method : "POST",
			body: JSON.stringify(newCategoryInput),
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then( res => {
			console.log(res)
			setIsLoading(false)
			if(res.status === 200){
				alert("Successfully added")
				return res.json()
			}else{
				alert("Invalid Credentials")
				throw new Error("Complete Fields Require")
			}
		})
		.then(data =>{
			setLastCreatedCategory(data)
		})
		.catch(err => console.log(err))
	}

	const handleChange = e => {
		setNewCategoryInput({
			...newCategoryInput,
			[e.target.id] : e.target.value
		})
	}






	return(
		<Card>
			<Card.Body>
				<Form onSubmit={handleSubmit}>
						<Form.Group controlId="category">
								<Form.Label>Category: </Form.Label>
								<Form.Control 
									type="text"
									onChange={handleChange}
									value={newCategoryInput.category}
								/>
						</Form.Group>

						<Form.Group controlId="description">
								<Form.Label>Description: </Form.Label>
								<Form.Control 
									type="text"
									onChange={handleChange}
									value={newCategoryInput.description}
								/>
						</Form.Group>	

						{ isLoading ? 
							<Button type="submit" block disabled>Submit</Button>
						: 	<Button type="submit" >Submit</Button>
						}	
				</Form>			
			</Card.Body>
		</Card>
	)
}

export default CategoryCreate;