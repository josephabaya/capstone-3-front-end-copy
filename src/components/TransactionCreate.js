import { useEffect , useState } from 'react'
import { Container,Col,Row, Card, Form, Button, Select } from 'react-bootstrap'
import CategoryListItem from './CategoryListItem'



const TransactionCreate = () => {

	const [ newTransactionInput , setNewTransactionInput ] = useState({
		"category" : "",
    	"transaction" : "",
    	"amount" : ""
	})


	const [ isLoading, setIsLoading ] = useState(false)


	const handleSubmit = e => {
		e.preventDefault()
		setIsLoading(true)
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/transaction', {
			method : 'POST',
			body: JSON.stringify(newTransactionInput),
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			}	
		})
		.then( res => {
			console.log(res)
			setIsLoading(false)
			if(res.status === 200){
				alert("Successfully added")
				return res.json()
			}else{
				alert("Invalid Credentials")
				throw new Error("Complete Fields Require")
			}
		})
		.then(data =>{
			console.log(data)
		})
		.catch(err => console.log(err))
	}



	let [ categories, setCategories ] = useState([])

	useEffect(() => {
		fetch('https://murmuring-ravine-84465.herokuapp.com/api/user/category',{
				headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then( res => res.json())
		.then( data =>{
			setCategories(data)
			console.log(categories)
		})
		.catch( err => console.log(err))
	},[])

	const categoryShow = categories.map((category ,index) =>{
		return(
			<option 
			value={category._id.name}
			key={index}
			>{category.category}
			</option>
		)
	})




	const handleChange = e => {
		setNewTransactionInput({
			...newTransactionInput,
			[e.target.id] : e.target.value
		})
	}


	return(

		<Container className="my-5" >
{/*			<Row>
				{categoryShow}
			</Row>*/}

			<Row className="justify-content-center">
				<Card>
					<Card.Body>
						<Form onSubmit={handleSubmit}>
								{/*<Form.Select
									controlId="category"
									select
									onChange={handleChange}
									>
								  <option>Choose Category</option>
								  {categoryDisplay}
							  <option value={newTransactionInput.category}>One</option>
								  {/*<option value="2">Two</option>
								  <option value="3">Three</option>*
								</Form.Select>*/}



								<Form.Group controlId="category">
										<Form.Label>Category: </Form.Label>
										<Form.Control 
											as="select"
											onChange={handleChange}
											value={newTransactionInput.category}
										>
										<option disabled>Choose Category</option>
										{categoryShow}
										</Form.Control>
								</Form.Group>







                               {/*<Form.Group controlId="category">
										<Form.Label>Category: </Form.Label>
										<Form.Control 
											type="text"
											onChange={handleChange}
											value={newTransactionInput.category}
										/>
								</Form.Group>*/}

								<Form.Group controlId="transaction">
										<Form.Label>Transaction: </Form.Label>
										<Form.Control 
											type="text"
											onChange={handleChange}
											value={newTransactionInput.transaction}
										/>
								</Form.Group>

								<Form.Group controlId="amount">
										<Form.Label>Amount: </Form.Label>
										<Form.Control 
											type="text"
											onChange={handleChange}
											value={newTransactionInput.amount}
										/>
								</Form.Group>	




								{ isLoading ? 
									<Button type="submit" block disabled>Submit</Button>
								: 	<Button type="submit" >Submit</Button>
								}	
						</Form>			
					</Card.Body>
				</Card>
			</Row>
		</Container>
	)

} 

export default TransactionCreate;


			// <Col className="my-2" xs={12} sm={6} md={4} lg={3} key={category._id}>

			// 	<CategoryListItem
			// 	category={category}
			// 	viewBtn={true}
			// 	/>

			// </Col>