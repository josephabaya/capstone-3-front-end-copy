import { Card, Button } from 'react-bootstrap'
import { ApplicationContext } from './../contexts/ApplicationContext'
import { useContext } from 'react'
import { Link } from 'react-router-dom'

export default function CategoryListItem({ categ, setLastDeletedProduct, viewBtn }){

	const { user } = useContext(ApplicationContext)

	const handleClickDelete = () => {
		console.log(categ._id)
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/category/${categ._id}`,{
			method : 'DELETE',
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then( res => res.json())
		.then( data => {
			setLastDeletedProduct(data)
		})
		.catch(err => console.log(err))
	}



	return(
		<Card>
			<Card.Body>
				{<Card.Title>{categ.category}</Card.Title>}
			    <Card.Text>{categ.description}</Card.Text>
				    {
				    viewBtn ?
				    <Button
				    as={Link}
				    to={`/categories/${categ._id}`}
				    className="my-1"
				    variant="primary"
				    block
				    >View</Button>
				    : <></>
				    }

				    {
				    viewBtn ?
				    <Button 
					    className="my-1" 	
					    variant="danger" 
					    onClick={handleClickDelete}
					    block
				    >
				    	Delete
				    </Button>
				    : <></>
				    }

{/*				    {
				   	!viewBtn ?
				   	<Button
				    className="my-1"
				    variant="primary"
				    block
				    >Edit</Button>
				    : <></>
				    }*/}
			</Card.Body>
		</Card>
	)
}