import 'bootstrap/dist/css/bootstrap.min.css';
import MainNav from './components/MainNav';
import ApplicationProvider from './contexts/ApplicationContext'
import Login from './pages/Login'
import Home from './pages/Home'
import Register from './pages/Register'
import Category from './pages/Category'
import CategoriesSingle from './pages/CategoriesSingle'
import Transaction from './pages/Transaction'


import Income from './pages/Income'
import Expenses from './pages/Expenses'

import IncomeSingle from './pages/IncomeSingle'
import ExpensesSingle from './pages/ExpensesSingle'

import { 
	BrowserRouter as Router,
	Switch,
	Route
} from 'react-router-dom'



function App() {
  return (
    <div className="App">
    <ApplicationProvider>
      <Router>  
        <MainNav/>
          <Switch>

            <Route exact path="/">
                <Home />
            </Route>

            <Route path="/login">
                <Login />
            </Route>

            <Route path="/register">
                <Register />
            </Route>

            <Route path="/category">
                <Category />
            </Route> 

            <Route path="/categories/:id">
              <CategoriesSingle />
            </Route> 

            <Route path="/transaction">
              <Transaction />
            </Route>

            <Route path="/income">
              <Income />
            </Route>

            <Route path="/expenses">
              <Expenses />
            </Route>

            <Route path="/profit/:id">
              <IncomeSingle />
            </Route>

            <Route path="/loss/:id">
              <ExpensesSingle />
            </Route>

          </Switch>
      </Router>
    </ApplicationProvider>
    </div>
  );
}

export default App;
