import { createContext, useState, useEffect } from 'react'

export const ApplicationContext = createContext()

export default function ApplicationProvider(props){

	const [ categories, setCategories] = useState([])
	
 // 	useEffect(() => {
	// 	fetch('http://localhost:4000/api/user/category',{
	// 			headers : {
	// 			'Authorization' : `Bearer ${localStorage.getItem('token')}` 
	// 		}
	// 	})
	// 	.then( res => {
	// 		console.log(res.json())
	// 	})
	// 	.then( data =>{
	// 		console.log(data)
	// 	})
	// 	.catch( err => console.log(err))
	// },[])


	const [user, setUser] = useState({
		userId : "",
		email : "",
		firstName : "",
		lastName : ""
	})

	useEffect( () => {
		if(!localStorage.token){
		}else{
			fetch('https://murmuring-ravine-84465.herokuapp.com/api/user', {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then( res => res.json())
		.then(data => {
			// console.log(data)
			let { firstName , lastName , email  } = data
			setUser({
				userId: data._id,
				firstName,
				lastName,
				email
			})
		})
		.catch( err => console.log(err))
		}

	}, [])

	return(
		<ApplicationContext.Provider
			value={{
				setUser,
				user
			}}
		>
			{props.children}
		</ApplicationContext.Provider>
	)
}




































