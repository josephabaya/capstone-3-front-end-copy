import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import ExpensesListItem from './../components/ExpensesListItem'
import { Container, Card, Row, Col, Spinner } from 'react-bootstrap'
import  ExpensesListUpdate  from './../components/ExpensesListUpdate'


export default function ExpensesSingle(){
	
	const { id } = useParams()
	const [ viewExpenses, setViewExpenses ] = useState({})
	const [ isLoading , setIsLoading ] = useState(true)
	const [ lastUpdatedExpenses , setLastUpdatedExpenses ] = useState({})


	useEffect(()=>{
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/expenses/${id}`,{
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			setViewExpenses(data)
			setIsLoading(false)
		})
		.catch(err => console.log(err))
	},[lastUpdatedExpenses])


	return(

		<Container className="my-5">
			<Row>
				<Col xs={12} sm={10} md={6} className="mx-auto">  
					{
						isLoading ?
						<Spinner animation="border" role="status">
						  <span className="sr-only">Loading please wait...</span>
						</Spinner>
						:
						<ExpensesListItem loss={viewExpenses}/>
					}
				</Col>
			</Row>

			<Card className="my-5">
				<Row>
					<Col>
						<ExpensesListUpdate loss={viewExpenses}  setLastUpdatedExpenses={setLastUpdatedExpenses}  />
					</Col>
				</Row>
			</Card>

		</Container>
	)
}