// import { Button } from 'react-bootstrap'
import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import CategoryListItem from './../components/CategoryListItem'
import { Container, Card, Row, Col, Spinner } from 'react-bootstrap'
import CategoryListUpdate from './../components/CategoryListUpdate'

export default function CategoriesSingle(){

	const { id } = useParams()
	const [ category, setCategory] = useState({})
	const [ isLoading , setIsLoading ] = useState(true)
	const [ lastUpdatedCategory , setLastUpdatedCategory ] = useState({})
 
	useEffect(()=> {
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/category/${id}`,{
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategory(data)
			setIsLoading(false)
		})
		.catch(err => console.log(err))
	},[lastUpdatedCategory])


	return(
		<Container className="my-5">
			<Row>
				<Col xs={12} sm={10} md={6} className="mx-auto">  
					{
						isLoading ?
						<Spinner animation="border" role="status">
						  <span className="sr-only">Loading please wait...</span>
						</Spinner>
						:
						<CategoryListItem categ={category}/>
					}
				</Col>
			</Row>

			<Card className="my-5">
				<Row>
					<Col>
						<CategoryListUpdate categ={category} setLastUpdatedCategory={setLastUpdatedCategory}/>
					</Col>
				</Row>
			</Card>
		</Container>
	)
}