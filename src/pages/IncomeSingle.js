import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import IncomeListItem from './../components/IncomeListItem'
import { Container, Card, Row, Col, Spinner } from 'react-bootstrap'
import IncomeListUpdate from './../components/IncomeListUpdate'

export default function IncomeSingle(){

	const { id } = useParams()
	const [ viewProfit, setViewProfit ] = useState({})
	const [ isLoading , setIsLoading ] = useState(true)
	const [ lastUpdatedIncome , setLastUpdatedIncome ] = useState({})


	useEffect(()=>{
		fetch(`https://murmuring-ravine-84465.herokuapp.com/api/user/income/${id}`,{
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			setViewProfit(data)
			setIsLoading(false)
		})
		.catch(err => console.log(err))

	},[lastUpdatedIncome])

	return(
		<Container className="my-5">
			<Row>
				<Col xs={12} sm={10} md={6} className="mx-auto">  
					{
						isLoading ?
						<Spinner animation="border" role="status">
						  <span className="sr-only">Loading please wait...</span>
						</Spinner>
						:
						<IncomeListItem profit={viewProfit}/>
					}
				</Col>
			</Row>

			<Card className="my-5">
				<Row>
					<Col>
						<IncomeListUpdate profit={viewProfit} setLastUpdatedIncome={setLastUpdatedIncome}/>
					</Col>
				</Row>
			</Card>
		</Container>
	)
}